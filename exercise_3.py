class House:
    def __init__(self):
        pass

    def room(self):
        print("First room")


class Bedroom(House):
    def __init__(self):
        pass

    def room(self):
        super(Bedroom, self).room()
        print("This is a bedroom")


this = Bedroom()
this.room()